package org.school.cicd;

class CalculatorTest {

    @org.junit.jupiter.api.Test
    void sum() {
        final Calculator calculator = new Calculator();
        assert calculator.sum(2, 2) == 4;
    }
}